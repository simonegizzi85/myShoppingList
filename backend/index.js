/* SETUP ================================================================================== */

/**
 * Required the needed package
**/
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');

/**
 * Require our models
**/
const Product = require('./models/product');

/**
 * Define our app to use express
**/
const app = express();

/**
 * Create our logger middleware
 **/
app.use(morgan('combined'));

/**
 * Set up connection to our remote MongoDB
**/
const credentials = {
    username: 'awesomeuser',
    password: 'aw3s0m3passw0rd'
};
mongoose.connect(`mongodb://${credentials.username}:${credentials.password}@ds147181.mlab.com:47181/myshoppinglist`, { useNewUrlParser: true });

/**
 * Use the environment variable PORT or default to 8080
**/
const port = process.env.PORT || 8080;

/**
 * Configure app to parse incoming request bodies with body-parser middleware
 * and make it available under the req.body property
**/
app.use(bodyParser.urlencoded({
    extended: true
}));

/**
 * Parse only JSON
 * Only looks at requests where the Content-Type header matches application/json
 * Set the maximum request body size to 50kb
**/
app.use(bodyParser.json({
    limit: '50kb',
    type: 'application/json'
}));

/* API ROUTER ============================================================================= */

/**
 * Get an instance of the express Router
**/
const router = express.Router();

/**
 * ONLY FOR DEMONSTRATION PURPOSE - DON'T DO IN PRODUCTION!!!
 * Allow request from all origin
 * Allow specific method
 */
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

/**
 * Define our RESTful API
**/
router.route('/products')
    .get((req, res) => {
        Product.find()
            .then((products) => {
                res.json(products);
            })
            .catch((err) => {
                res.send(err);
            });
    })
    .post((req, res) => {
        if(!req.body.name){
            res.json({response:"KO", message:"Il nome del prodotto è obbligatorio"});
        }
        let product = new Product();
        product.name = req.body.name;
        if ( req.body.hasOwnProperty('owned') ) {
            product.owned = req.body.owned;
        }
        product.save()
            .then(() => {
                res.json({response:"OK", message:"Prodotto inserito!"});
            })
            .catch((err) => {
                res.send(err);
            });
    });

router.route('/products/:product_id')
    .get((req, res) => {
        Product.findById(req.params.product_id)
            .then((product) => {
                res.json(product);
            })
            .catch((err) => {
               res.send(err);
            });
    })
    .put((req, res) => {
       Product.findByIdAndUpdate(req.params.product_id, req.body, {new: true})
           .then((product) => {
                res.json({response:"OK", message:"Prodotto aggiornato!", product: product});
           })
           .catch((err) => {
               res.send(err);
           })
    })
    .delete((req, res) => {
        Product.remove({
            _id: req.params.product_id
        })
            .then(() => {
                res.json({response:"OK", message:'Prodotto rimosso!'});
            })
            .catch((err) => {
                res.send(err);
            })
    });

/* REGISTER OUR ROUTE ===================================================================== */

/**
 * Prefix our RESTful API with /api/v1 suffix
**/
app.use('/api/v1', router);

/* START SERVER =========================================================================== */
app.listen(port);
console.log(`Server listen on port ${port}!`);