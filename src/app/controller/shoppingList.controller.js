(function(){

    "use strict";
    
    angular
        .module('myShoppingList')
        .controller('ShoppingListController', ['$scope', 'ProductsAPI', '$timeout', ShoppingListController]);

    function ShoppingListController(scope, ProductsAPI) {
        /**
         * Bootstrapping
         */
        scope.products = [];
        scope.searching = '';
        scope.summary = {
            toBuy: 0,
            buyed: 0,
            totalAmount: 0,
            remainingAmount: 0
        };
        scope.temp = {
            name: null,
            price: null
        };

        /**
         * C.R.U.D. method
        **/
        scope.storeNewProduct = function(e){
            if (e.which !== 13){
                return e;
            }
            ProductsAPI.storeProduct(scope.newProduct)
                .then(function(response){
                    if(response === 'OK'){
                        scope.getProductsList();
                    }
                });
        };
        scope.getProductsList = function(){
            ProductsAPI.getProducts()
                .then(function(response){
                    scope.products = response;
                })
                .finally(function(){
                    scope.newProduct = {
                        name: null,
                        owned: false,
                        price: 0
                    };
                    scope.$emit('The list has changed');
                });
        };
        scope.updateProduct = function(product){
            ProductsAPI.updateProduct(product)
                .then(function(response){
                    _resetAllEditFields();
                    scope.$emit('The list has changed');
                });
        };
        scope.trashProduct = function(productId){
            ProductsAPI.deleteProduct(productId)
                .then(function(response){
                    if(response === 'OK'){
                        scope.getProductsList();
                    }
                });
        };

        /**
         * Helper method
        **/
        scope.ownedProductToggle = function(product){
            product.owned = !product.owned;
            scope.updateProduct(product);
        };
        scope.editName = function(product){
            product.editingName = true;
            _resetEditField(product);
        };
        scope.saveName = function(product){
            scope.updateProduct(product);
        };
        scope.saveNameOnEnter = function(e, product){
            if (e.which !== 13){
                return e;
            }
            scope.saveName(product);
            e.target.blur();
        };

        /**
         * Event watcher
        **/
        scope.$on('The list has changed', function(e){
            var toBuy = 0,
                buyed = 0,
                totalAmount = 0,
                remainingAmount = 0;
            scope.products.forEach(function(el){
                totalAmount += el.price;
                if(el.owned){
                    buyed++;
                    remainingAmount -= el.price;
                } else {
                    toBuy++;
                    remainingAmount += el.price;
                }
            });
            scope.summary.toBuy = toBuy;
            scope.summary.buyed = buyed;
            scope.summary.totalAmount = totalAmount;
            scope.summary.remainingAmount = remainingAmount;
        });

        /**
         * Private method
        **/
        function _resetEditField(product){
            scope.products.forEach(function(el){
                if(el !== product){
                    el.editingName = false;
                    el.editingPrice = false;
                }
            });
        }
        function _resetAllEditFields(){
            scope.temp = {
                name: null,
                price: null
            };
            scope.products.forEach(function(el){
                el.editingName = false;
                el.editingPrice = false;
            });
        }
        
        /**
         * Get products list at application startup
        **/
        scope.getProductsList();
    }

})();