(function(){

    "use strict";

    angular
        .module('myShoppingList')
        .factory('ProductsAPI', ['api', '$http', ProductsAPI]);

    function ProductsAPI(api, $http) {
        return{
            getProducts: function(){
                return $http.get(api.endpoint + 'products')
                    .then(function(response){
                        response.data.forEach(function(el){
                            el.editingName = false;
                        });
                        return response.data;
                    })
                    .catch(function(error){
                        return error;
                    });
            },
            storeProduct: function(product){
                return $http.post(api.endpoint + 'products', product)
                    .then(function(response){
                        return response.data.response;
                    })
                    .catch(function(error){
                        return error;
                    });
            },
            updateProduct: function(product){
                return $http.put(api.endpoint + 'products/' + product._id, product)
                    .then(function(response){
                        return response.data;
                    })
                    .catch(function(error){
                        return error;
                    });
            },
            deleteProduct: function(productId){
                return $http.delete(api.endpoint + 'products/' + productId)
                    .then(function(response){
                        return response.data.response;
                    })
                    .catch(function(error){
                        return error;
                    });
            }
        }
    }

})();