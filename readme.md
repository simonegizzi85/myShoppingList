# myShoppingList

An example web-app for create, read, update and delete products on your personal shopping list.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

What you need on your machine:
* [Node.js](https://nodejs.org/it/)

## Installation

The installation is divided into two steps. You need to install the server to use the application.

So, first of all, clone this repository, then:

## Installing webapp

```
npm install
```

## Installing RESTful api server

```
cd backend && npm install
```

## Launch webapp

```
npm start
```

## Launch RESTful api server

```
cd backend && npm start
```

Now, enjoy the app :) 

## Built on top

* [Node.js](https://nodejs.org/it/) - JavaScript runtime built on Chrome's V8 JavaScript engine
* [Npm](https://www.npmjs.com/) - The Node.js Package Manager
* [Express](https://expressjs.com/it/) - Node.js framework
* [MongoDB](https://www.mongodb.com/) - The document database offered by MLab
* [Mongoose](http://mongoosejs.com/) - Mongodb object modeling for node.js
* [MLab](https://mlab.com/) - The hosting platform of Mongo i've used
* [AngularJS](https://angularjs.org/) - A Javascript MV* framework made by Google
* [Sass](https://sass-lang.com/) - A CSS pre-processor
* [Gulp](https://gulpjs.com/) - A toolkit for automating tasks

## Versioning

I've use [GitLab](https://gitlab.com/) for versioning.

## Authors

* **Simone "Guizzo" Gizzi** - *Full-Stack Developer*
